# README #

This is the instruction for this repository.

### What is this repository for? ###

This repository is for the an application that allows the user place an order using a mobile device and provide payment details, the payment details will be saved on firebase.
It is written in JavaScript and node.js.
This is Version 1.2 
The full tutorials will be made available on request.


### How do I get set up? ###

Kindly initiate the server from visual studio codes.
Open a terminal and type "npm start" in the terminal. 
Please open http://localhost:3002 from your browser.
The dependencies include express, express-validator, body-parser and ejs. 
The inputs will be saved in firebase.


### Contribution guidelines ###

The test cases have been completely developed. Code review is done every other week.


### Who do I talk to? ###

Please contact bunmioluwa@gmail.com for more details about the repository or any further clarifications.

### License ###

Subject to the terms of this agreement, Bunmi Abioye hereby grants to the Licensee a personal, non-transferable, non-exclusive license to use this application only for Authorized Use.

The licensee is not permitted to: Edit, alter, modify, adapt, translate or otherwise change the whole or any part of the software nor permit the whole or any part of the software to be combined with or become incorporated in any other software, nor decompile, disassemble or reverse engineer the Apps or attempt to do any such things. Reproduce, copy, distribute, resell or otherwise use the Apps for any commercial purpose. Allow any third party to use the Apps on behalf of or for the benefit of any third party. Use the Apps in any way which breaches any applicable local, national or international law.

### Intellectual Property and Ownership ###

Bunmi Abioye shall at all times retain ownership of the Apps as originally downloaded by Licensee and all subsequent downloads of the App by Licensee.


